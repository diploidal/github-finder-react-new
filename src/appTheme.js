export const appTheme = {
  palette: {
    primary: {
      main: '#000000',
    },
    secondary: {
      main: '#D3D3D3',
    },
    error: {
      main: "#FF0000",
    },
    // TODO: add more theme options
  }
}