import React from 'react';
import { useStyles } from './App.jss'
import { SearchContainer } from './Components/SearchContainer';


export const App = () => {
  const classes = useStyles();
  return (
    <div className={classes.app}>
      <h1 className={classes.headerText}>Hello there !</h1>
      <SearchContainer/>
    </div>
  );
}
