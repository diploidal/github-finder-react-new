import { createUseStyles } from 'react-jss'

export const useStyles = createUseStyles(theme => ({
  app: {
    // 'text-align': 'center',
  },
  headerText: {
    'color': theme.palette.primary.main,
    'background-color': theme.palette.secondary.main,
  },
}))