import { createUseStyles } from 'react-jss'

export const useStyles = createUseStyles(theme => ({
  card: {
    border: '2px solid black',
    borderRadius: '6px',
    padding: '20px 20px',
    display: 'flex',
    '& img': {
      minWidth: '312px',
      maxWidth: '100%',
    },
  },
  information: {
    // TODO make account information reactive, jump below image on mobile
    padding: '20px',
  }
}))