import React from 'react';
import { useStyles } from './Card.jss'

export const Card = (props) => {
  const classes = useStyles();
  return (
    <div className={classes.card}>
      <img className={classes.image} src={props.userData.avatar_url} alt="user"/>
      <div className={classes.information}>
        <span>Name: {props.userData.name}</span><br/> 
        <span>Login: {props.userData.login}</span><br/>
        <span>Location: {props.userData.location}</span><br/> 
        <span>User ID: {props.userData.id}</span><br/>
        <span>Company: {props.userData.company}</span><br/>
        <span>Has {props.userData.followers} followers</span><br/>
        <span>Follows {props.userData.following} users</span><br/>
        <span>User ID: {props.userData.id}</span><br/>
        <span>{props.userData.public_repos} public repositories</span><br/>
        <span>Last update - {props.userData.updated_at}</span><br/>
      </div>
    </div>
  )
}