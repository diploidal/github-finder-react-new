import { createUseStyles } from 'react-jss'

export const useStyles = createUseStyles(theme => ({
  container: {
      margin: '0 auto',
      maxWidth: '80%',
      padding: '20px 20px',
  }
}))